import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('Frontend Technical Test');
  });

  it('should contain logo component', () => {
    expect(page.getAppLogo()).toBeTruthy();
  });

  it('should contain todo page component', () => {
    expect(page.getTodoPage()).toBeTruthy();
  });

  it('should display message for todo list', () => {
    expect(page.getTodoListMessages().first().getText()).toEqual('Todo List');
  });

  it('should display message for archived todo list', () => {
    expect(page.getTodoListMessages().last().getText()).toEqual('Archived List');
  });

  it('should correct set text for new todo', () => {
    const text = 'Some text';
    page.getTodoInput().sendKeys(text);
    expect(page.getTodoInput().getAttribute('value')).toEqual(text);
  });

  it('should clear input after adding new todo', () => {
    page.getTodoInput().sendKeys('Some text');
    page.getTodoButton().click();
    expect(page.getTodoInput().getAttribute('value')).toEqual(''); // 7 todos
  });

  it('should undone todo', () => {
    const todo = page.getDoneTodo().first().getWebElement();
    todo.click();
    expect(todo.getAttribute('class')).not.toContain('done');
  });

  it('should done todo', () => {
    const todo = page.getNotDoneTodo().first().getWebElement();
    todo.click();
    expect(todo.getAttribute('class')).toContain('done');
  });

  it('should not be unarchived and archived elements at the same time', () => {
    expect(page.getArchivedAndUnarchivedElements().count()).toEqual(0);
  });

  it('should remove todo', () => {
    const removeBtn = page.getRemoveBtn();
    removeBtn.click();
    expect(page.getTodos().count()).toEqual(6);
  });
});
