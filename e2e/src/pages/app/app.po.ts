import { browser, by, element, ElementArrayFinder, ElementFinder } from 'protractor';

export class AppPage {
  public navigateTo() {
    return browser.get('/');
  }

  public getTitleText() {
    return element(by.css('app-root h1')).getText();
  }

  public getAppLogo(): ElementFinder {
    return element(by.tagName('app-logo'));
  }

  public getTodoPage(): ElementFinder {
    return element(by.tagName('app-todo-page'));
  }

  public getTodoListMessages(): ElementArrayFinder {
    return element.all(by.css('.content h3'));
  }

  public getTodoInput(): ElementFinder {
    return element(by.css('input[name="todoText"]'));
  }

  public getTodoButton(): ElementFinder {
    return element(by.css('input[type="submit"]'));
  }

  public getTodos(): ElementArrayFinder {
    return element.all(by.css('article'));
  }

  public getDoneTodo(): ElementArrayFinder {
    return element.all(by.css('article.done'));
  }

  public getNotDoneTodo(): ElementArrayFinder {
    return element.all(by.css('article:not(.done)'));
  }

  public getArchivedAndUnarchivedElements(): ElementArrayFinder {
    return element.all(by.css('article .archive.unarchive'));
  }

  public getRemoveBtn(): ElementArrayFinder {
    return element.all(by.css('.remove'));
  }
}
