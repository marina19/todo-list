import { TestBed, async }    from '@angular/core/testing';
import { FormsModule }       from '@angular/forms';
import { BrowserModule }     from '@angular/platform-browser';
import { AppComponent }      from './app.component';
import { AppLogoComponent }  from './features/todo/components/app-logo/app-logo.component';
import { TodoListComponent } from './features/todo/components/todo-list/todo-list.component';
import { TodoPageComponent } from './features/todo/components/todo-page/todo-page.component';
import { ArchivedTodoPipe }  from './features/todo/pipes/archived-todo/archived-todo.pipe';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        AppLogoComponent,
        TodoPageComponent,
        TodoListComponent,
        ArchivedTodoPipe
      ],
      imports: [
        BrowserModule,
        FormsModule]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
