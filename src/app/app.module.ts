import { BrowserModule }    from '@angular/platform-browser';
import { NgModule }         from '@angular/core';
import { AppComponent }     from './app.component';
import { AppLogoComponent } from './shared/components/app-logo';
import { TodoModule }       from './features/todo/todo.module';

@NgModule({
  declarations: [
    AppComponent,
    AppLogoComponent,
  ],
  imports: [
    BrowserModule,
    TodoModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
