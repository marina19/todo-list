import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule }                      from '@angular/forms';
import { BrowserModule }                    from '@angular/platform-browser';
import { ArchivedTodoPipe }                 from '../../pipes/archived-todo/archived-todo.pipe';
import { TodoListComponent }                from './todo-list.component';
import InitialTodos                         from '../todo-page/initial-todos';

describe('TodoListComponent', () => {
  let component: TodoListComponent;
  let fixture: ComponentFixture<TodoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TodoListComponent,
        ArchivedTodoPipe
      ],
      imports: [
        BrowserModule,
        FormsModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoListComponent);
    component = fixture.componentInstance;
    component.todos = InitialTodos;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call archive event', () => {
    spyOn(component.archiveTodo, 'emit');
    component.onArchiveTodo(component.todos[0]);
    expect(component.archiveTodo.emit).toHaveBeenCalled();
  });

  it('should call unarchive event', () => {
    spyOn(component.unarchiveTodo, 'emit');
    component.onUnarchiveTodo(component.todos[0]);
    expect(component.unarchiveTodo.emit).toHaveBeenCalled();
  });

  it('should call toggle event', () => {
    spyOn(component.toggleTodo, 'emit');
    component.onToggleTodo(component.todos[0]);
    expect(component.toggleTodo.emit).toHaveBeenCalled();
  });

  it('should call remove event', () => {
    spyOn(component.removeTodo, 'emit');
    component.onRemoveTodo(component.todos[0]);
    expect(component.removeTodo.emit).toHaveBeenCalled();
  });
});
