import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
}                from '@angular/core';
import ITodoItem from '../../models/todo-item.model';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {

  @Input() todos: ITodoItem[];
  @Input() title: string;
  @Input() isArchived: boolean;

  @Output() archiveTodo: EventEmitter<any> = new EventEmitter();
  @Output() unarchiveTodo: EventEmitter<any> = new EventEmitter();
  @Output() removeTodo: EventEmitter<any> = new EventEmitter();
  @Output() toggleTodo: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  public onArchiveTodo(todo: ITodoItem): void {
    this.archiveTodo.emit(todo);
  }

  public onUnarchiveTodo(todo: ITodoItem): void {
    this.unarchiveTodo.emit(todo);
  }

  public onRemoveTodo(todo: ITodoItem): void {
    this.removeTodo.emit(todo);
  }

  public onToggleTodo(todo: ITodoItem): void {
    this.toggleTodo.emit(todo);
  }
}
