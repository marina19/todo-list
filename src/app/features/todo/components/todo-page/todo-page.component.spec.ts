import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule }                      from '@angular/forms';
import { BrowserModule }                    from '@angular/platform-browser';
import { ArchivedTodoPipe }                 from '../../pipes/archived-todo/archived-todo.pipe';
import { TodoListComponent }                from '../todo-list/todo-list.component';
import InitialTodos                         from './initial-todos';

import { TodoPageComponent } from './todo-page.component';

describe('TodoPageComponent', () => {
  let component: TodoPageComponent;
  let fixture: ComponentFixture<TodoPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TodoPageComponent,
        TodoListComponent,
        ArchivedTodoPipe
      ],
      imports: [
        BrowserModule,
        FormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoPageComponent);
    component = fixture.componentInstance;
    component.todos = InitialTodos;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should archive todo', () => {
    component.archiveTodo(component.todos[0]);
    expect(component.todos[0].archived).toBeTruthy();
  });

  it('should unarchive todo', () => {
    component.unarchiveTodo(component.todos[0]);
    expect(component.todos[0].archived).toBeFalsy();
  });

  it('should create todo', () => {
    const lengthBeforeAdd = component.todos.length;
    const newText = 'New todo';
    component.todoText = newText;
    component.createTodo();
    expect(component.todos.length).toBe(lengthBeforeAdd + 1);
    expect(component.todos[lengthBeforeAdd].text).toBe(newText);
  });

  it('should remove first todo', () => {
    const removedIndex = 0;
    const removedTodo = component.todos[removedIndex];
    component.removeTodo(removedTodo);
    expect(component.todos[removedIndex]).not.toEqual(removedTodo);
  });

  it('should remove todo in the middle', () => {
    const removedIndex = 2;
    const removedTodo = component.todos[removedIndex];
    component.removeTodo(removedTodo);
    expect(component.todos[removedIndex]).not.toEqual(removedTodo);
  });

  it('should remove last todo', () => {
    const lastIndex = component.todos.length - 1;
    const removedTodo = component.todos[lastIndex];
    component.removeTodo(removedTodo);
    expect(component.todos[lastIndex]).not.toEqual(removedTodo);
  });

  it('should toggle todo', () => {
    const toggledTodo = component.todos[0];
    const isDone = toggledTodo.done;
    component.toggleTodo(toggledTodo);
    expect(toggledTodo.done).not.toEqual(isDone);
  });

  it('should build todo', () => {
    const newTodoText = 'New Todo';
    const lastTodo = component.buildTodo(newTodoText);
    expect(lastTodo.id).toEqual(component.lastTodoIndex - 1);
    expect(lastTodo.text).toEqual(newTodoText);
    expect(lastTodo.done).toBeFalsy();
    expect(lastTodo.archived).toBeFalsy();
  });
});
