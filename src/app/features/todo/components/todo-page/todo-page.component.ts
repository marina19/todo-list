import { Component, OnInit } from '@angular/core';
import ITodoItem             from '../../models/todo-item.model';
import InitialTodos          from './initial-todos';

@Component({
  selector: 'app-todo-page',
  templateUrl: './todo-page.component.html',
  styleUrls: ['./todo-page.component.scss']
})
export class TodoPageComponent implements OnInit {
  public lastTodoIndex = 1;
  public todos = InitialTodos;
  public todoText = '';

  constructor() {
  }

  ngOnInit() {
    this.lastTodoIndex = (this.todos.length !== 0) ? this.todos.length + 1 : 1;
  }

  public archiveTodo(todo: ITodoItem): void {
    if (todo.archived) {
      return;
    }
    todo.archived = true;
  }

  public createTodo(): void {
    if (this.todoText.length <= 0) {
      return;
    }
    this.todos.push(this.buildTodo(this.todoText));
    this.todoText = '';

  }

  public removeTodo(todo: ITodoItem): void {
    const todoIndex = this.todos.indexOf(todo);
    if (todoIndex === -1) {
      return;
    }
    this.todos.splice(todoIndex, 1);
  }

  public toggleTodo(todo: ITodoItem): void {
    if (todo.archived) {
      return;
    }
    todo.done = !todo.done;

  }

  public unarchiveTodo(todo: ITodoItem): void {
    if (!todo.archived) {
      return;
    }
    todo.archived = false;
  }

  public buildTodo(text: string): ITodoItem {
    return {
      id: this.lastTodoIndex++,
      text,
      done: false,
      archived: false
    };
  }

  public trackByFn(index: number): number {
    return index;
  }
}
