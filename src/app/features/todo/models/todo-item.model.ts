interface ITodoItem {
  id: number;
  text: string;
  done: boolean;
  archived: boolean;
}

export default ITodoItem;
