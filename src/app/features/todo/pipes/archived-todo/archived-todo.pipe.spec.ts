import { ArchivedTodoPipe } from './archived-todo.pipe';

describe('ArchivedTodoPipe', () => {
  const pipe = new ArchivedTodoPipe();
  const todos = [{
    id: 1,
    text: 'Buy paper towels',
    done: false,
    archived: false
  }, {
    id: 2,
    text: 'Call the print lab',
    done: false,
    archived: false
  }, {
    id: 3,
    text: 'Look out the window',
    done: true,
    archived: true
  }];

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('transforms archived todos', () => {
    expect(pipe.transform(todos, true).length).toEqual(1);
  });

  it('transforms unarchived todos', () => {
    expect(pipe.transform(todos, false).length).toEqual(2);
  });
});
