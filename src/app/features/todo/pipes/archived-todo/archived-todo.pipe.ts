import { Pipe, PipeTransform } from '@angular/core';
import ITodoItem               from '../../models/todo-item.model';

@Pipe({
  name: 'archived',
  pure: false
})
export class ArchivedTodoPipe implements PipeTransform {

  transform(todos: ITodoItem[] = [], isArchived: boolean): any {
    return todos.filter(todo => todo.archived === isArchived);
  }

}
