import { NgModule }          from '@angular/core';
import { CommonModule }      from '@angular/common';
import { FormsModule }       from '@angular/forms';
import { TodoListComponent } from './components/todo-list';
import { TodoPageComponent } from './components/todo-page';
import { ArchivedTodoPipe }  from './pipes/archived-todo';

@NgModule({
  declarations: [
    TodoListComponent,
    ArchivedTodoPipe,
    TodoPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    TodoPageComponent
  ]
})
export class TodoModule { }
